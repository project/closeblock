<?php

/**
 *
 */
function closeblock_system_theme_settings(&$form, $form_state) {
  $settings = variable_get($form['var']['#value'], array());
  $settings = array_merge(closeblock_default_settings(), $settings, array());

  $form['closeblock'] = array(
    '#type' => 'fieldset',
    '#title' => t('Closeblock selectors'),
    '#weight' => 0,
    '#attributes' => array('id' => 'closeblock_form'),
  );
  
  $form['closeblock']['closeblock_type'] = array(
    '#type' => 'radios',
    '#title' => t('Block close behavior'),
    '#options' => array(1 => t('None.'), 2 => t('Slide Up.'), 3 => t('Fade Out')),
    '#default_value' => $settings['closeblock_type'],
  );
  
  $form['closeblock']['closeblock_speed'] = array(
    '#type' => 'select',
    '#title' => t('Animation speed'),
    '#options' => drupal_map_assoc(array('50', '100', '200', '300', '400', '500', '700', '1000', '1300')),
    '#description' => t('The animation speed in milliseconds.'),
    '#default_value' => $settings['closeblock_speed'],
  );
  
  $form['closeblock']['closeblock_save'] = array(
    '#type' => 'checkbox',
    '#title' => t('Save state for user'),
    '#default_value' => $settings['closeblock_save'],
  );
}

/**
 *
 */
function closeblock_block_admin_configure(&$form, $form_state) {
  $settings = variable_get('closeblock_settings', array());
  $form['#submit'][] = 'closeblock_submit';
  $form['visibility']['closeblock'] = array(
    '#type' => 'fieldset',
    '#title' => t('Closeblock'),
    '#collapsible' => FALSE,
    '#tree' => TRUE,
    '#weight' => 10,
  );
  
  $params = array(
    'module' => $form['module']['#value'],
    'delta' => $form['delta']['#value'],
  );
  $block_id = theme('closeblock_block_id', $params);
  
  if (isset($settings[$block_id])) {
    $default_values = $settings[$block_id];
  } else {
    $default_values = array();
  }
  
  $form['visibility']['closeblock']['closeblock_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use close button'),
    '#default_value' => !empty($default_values['closeblock_active']) ? $default_values['closeblock_active'] : '',
    '#description' => t('Advanced settings <a href="@link">here.</a>', array(
      '@link' => url('admin/appearance/settings'),
    )),
  );
}

/**
 * Form submission handler for block_admin_configure().
 *
 * @see block_admin_configure()
 * @see closeblock_form_alter()
 */
function closeblock_submit($form, &$form_state) {
  $settings = variable_get('closeblock_settings', array());
  $params = array(
    'module' => $form_state['values']['module'],
    'delta' => $form_state['values']['delta'],
  );
  $block_id = theme('closeblock_block_id', $params);
  $settings[$block_id] = $form_state['values']['closeblock'];
  $settings[$block_id]['module'] = $form_state['values']['module'];
  $settings[$block_id]['delta'] = $form_state['values']['delta'];
  variable_set('closeblock_settings', $settings);
}
