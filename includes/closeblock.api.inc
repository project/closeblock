<?php

/**
 * Default theme's settings
 */
function closeblock_default_settings() {
  $defaults = array(
    'closeblock_type' => CLOSEBLOCK_TYPE,
    'closeblock_speed' => CLOSEBLOCK_SPEED,
    'closeblock_save' => CLOSEBLOCK_SAVE,
  );
  return $defaults;
}

/**
 * Load settings
 */
function closeblock_initialization() {
  if (user_access('closeblock')) {
    global $theme, $user;
    
    $current_theme = $theme ? $theme : variable_get('theme_default', 'garland');
    
    $params = array(
      'uid' => $user->uid,
      'theme' => $current_theme,
    );
    
    $blocks = closeblock_closing_info_get($params);
    
    drupal_static('closeblock_blocks', $blocks);
    
    //load the settings
    global $theme;
    $current_theme = $theme ? $theme : variable_get('theme_default', 'garland');
    $theme_settings = variable_get(str_replace('/', '_', 'theme_' . $current_theme . '_settings'), array());
    $theme_settings = array_merge(closeblock_default_settings(), $theme_settings, array());
    
    drupal_static('closeblock_theme_settings', $theme_settings);
  }
}

/**
 * Write block settings to Drupal.settings.closeblock
 */
function closeblock_ajax_settings($data, $block) {
  if (user_access('closeblock') && !empty($data['content'])) {
    $theme_settings = drupal_static('closeblock_theme_settings');
    $blocks = drupal_static('closeblock_blocks');
    
    $settings = variable_get('closeblock_settings', array());
    
    $params = (array) $block;
    
    $block_id = theme('closeblock_block_id', $params);
    
    if (empty($settings[$block_id]['closeblock_active'])) {
      return;
    }
    
    $closed = !empty($blocks[$block_id]) && !empty($theme_settings['closeblock_save']);
    
    $types = array(1 => '', 2 => 'slideUp', 3 => 'fadeOut');
      
    drupal_add_js(array('closeblock' => array(
      $block_id => array(
        'closed' => $closed,
        'speed' => $theme_settings['closeblock_speed'],
        'save' => $theme_settings['closeblock_save'],
        'type' => $types[$theme_settings['closeblock_type']],
      )
    )), array('type' => 'setting', 'scope' => JS_DEFAULT));
    
    $path = drupal_get_path('module', 'closeblock');
    
    drupal_add_js($path .'/theme/js/closeblock.js');
    drupal_add_css($path .'/theme/css/closeblock.css');
    
    drupal_add_js('misc/ajax.js');
  }
}

/**
 * Set info.
 */
function closeblock_closing_info_set($params) {
  if (isset($params['uid']) && !empty($params['uid'])) {
    $info = closeblock_closing_info_get($params);
  
    $params['count'] = 1;
    $params['timestamp'] = time();
    
    if ($info) {
      $params['count'] = $info[0]->count + 1;
      drupal_write_record('closeblock', $params, array('module', 'delta', 'theme', 'uid'));
    }
    else {
      drupal_write_record('closeblock', $params);
    }
  }
  else {
    closeblock_closing_info_anonymous_set($params);
  }
}

/**
 * Get info.
 */
function closeblock_closing_info_get($params) {
  if (isset($params['uid']) && empty($params['uid'])) {
    return closeblock_closing_info_anonymous_get($params);
  }
  
  if (empty($params) || !is_array($params)) {
    return;
  }
  
  $result = closeblock_closing_info_get_db($params);
  
  $blocks = array();
  
  if (!empty($result)) {
    foreach ($result as $block) {
      $params = (array) $block;
      $block_id = theme('closeblock_block_id', $params);
      $blocks[$block_id] = $block;
    }
  }
  
  return $blocks;
}

/**
 * Set info for anonymous users.
 */
function closeblock_closing_info_anonymous_set($params) {
  if (!empty($params['theme']) && !empty($params['module']) && !empty($params['delta'])) {
    $block_id = theme('closeblock_block_id', $params);
    
    $_SESSION['closeblock'][$block_id] = array(
      'module' => $params['module'],
      'delta' => $params['delta'],
    );
  }
}

/**
 * Get info for anonymous users.
 */
function closeblock_closing_info_anonymous_get($params) {
  if (!empty($params['theme']) && !empty($params['module']) && !empty($params['delta'])) {
    $block_id = theme('closeblock_block_id', $params);
    
    if (!empty($_SESSION['closeblock'][$block_id])) {
      return $_SESSION['closeblock'][$block_id];
    }
  }
  else if (!empty($_SESSION['closeblock'])) {
    return $_SESSION['closeblock'];
  }
  
  return array();
}

/**
 * Ajax callback function.
 */
function closeblock_ajax_callback($module, $delta) {
  global $theme, $user;
  $current_theme = $theme ? $theme : variable_get('theme_default', 'garland');
  $params = array(
    'uid' => $user->uid,
    'theme' => $current_theme,
    'module' => $module,
    'delta' => $delta,
  );
  closeblock_closing_info_set($params);
}
