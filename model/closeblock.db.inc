<?php

/**
 * Get info.
 */
function closeblock_closing_info_get_db($params) {
  $query = db_select('closeblock', 'closeblock')
    ->fields('closeblock');
    
  foreach ($params as $param=>$value) {
    $query->condition($param, $value,'=');
  }
  
  $result = $query->execute();
  $result = $result->fetchAll();
  
  return $result;
}